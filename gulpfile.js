/**
 * Created by user on 15/6/24.
 */
var gulp = require('gulp');
var gutil = require('gulp-util');
//var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
//var sh = require('shelljs');
var watch = require('gulp-watch');


var minifyJS = require('gulp-uglify');

var paths = {
    sass: ['./app/sass/*.scss']
};

gulp.task('app-sass', function() {
    gulp.src('./app/sass/style.scss')
        .pipe(sass())
        .pipe(gulp.dest('./app/css/'))
        .pipe(minifyCss({
            keepSpecialComments: 0
        }))
        .pipe(rename({ extname: '.min.css' }))
        .pipe(gulp.dest('./app/css/'))
});

gulp.task('dist-sass', function() {
    gulp.src('./app/sass/style.scss')
        .pipe(sass())
        .pipe(gulp.dest('./dist/css/'))
        .pipe(minifyCss({
            keepSpecialComments: 0
        }))
        .pipe(rename({ extname: '.min.css' }))
        .pipe(gulp.dest('./dist/css/'))
});
//gulp.task('custom-sass',function(){
//    gulp.src('./www/css/custom/app.scss')
//        .pipe(sass())
//        .pipe(gulp.dest('./www/css/'))
//        .pipe(minifyCss({
//            keepSpecialComments: 0
//        }))
//        .pipe(rename({ extname: '.min.css' }))
//        .pipe(gulp.dest('./www/css/'))
//});

gulp.task('css-watch', function() {
    gulp.watch('./app/sass/*.scss',['dist-sass']);
    gulp.watch('./app/sass/*.scss',['app-sass']);
});

gulp.task('js',function(){
    gulp.src('./app/js/app.js')
        .pipe(gulp.dest('./dist/js/'))
        .pipe(minifyJS({
            mangle:false
        }))
        .pipe(rename({ extname: '.min.js' }))
        .pipe(gulp.dest('./dist/js/'))
});
gulp.task('img',function(){
    gulp.src('./app/img/*.*')
        .pipe(gulp.dest('./dist/img/'));
});
//gulp.task('js-lib',function(){
//    gulp.src(['./www/lib/ionic/js/ionic.bundle.js','./www/lib/ionic/js/angular/angular-resource.js','./www/lib/ionic/js/angular/angular-cookie.js'])
//        .pipe(concat('lib.js'))
//        .pipe(gulp.dest('./www/dist/'))
//        .pipe(minifyJS({
//            mangle:false
//        }))
//        .pipe(rename({ extname: '.min.js' }))
//        .pipe(gulp.dest('./www/dist/'))
//});


gulp.task('js-watch',function(){
    gulp.watch('./app/js/*.js',['js']);
});

//gulp.task('html',function(){
//    gulp.src([
//        './www/templates/manage/*.htm',
//        './www/templates/modules/*.htm',
//        './www/templates/personal/*.htm'
//    ])
//        .pipe(minifyHTML({
//            quotes: true
//        }))
//        .pipe(templateCache())
//        .pipe(chinese2unicode())
//        .pipe(gulp.dest('./www/dist/'));
//});

//gulp.task('watch-html',function(){
//    gulp.watch('./www/templates/**/*.htm',['html']);
//});

gulp.task('default', ['app-sass','dist-sass','css-watch','js','js-watch','img']);
