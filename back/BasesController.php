<?php namespace App\Http\Controllers;

use DB;
use App\Quotation;
class BasesController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Welcome Controller
    |--------------------------------------------------------------------------
    |
    | This controller renders the "marketing page" for the application and
    | is configured to only allow guests. Like most of the other sample
    | controllers, you are free to modify or remove it as you desire.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application welcome screen to the user.
     *
     * @return Response
     */
    public function index()
    {
        $cases = array();
        $results = DB::select('SELECT a.t_id,a.t_title,a.t_shortDesc,a.t_publishDate From topic a  where a.t_channel_id = "8" order by a.t_publishDate desc');
    //  var_dump($results);exit;
        foreach($results as $key){
            $cases[] = array(
                'id'=>$key->t_id,
                'title'=>$key->t_title,
                'desc'=>$key->t_shortDesc,
                'pDate'=>$key->t_publishDate,
                            );
        }
        return view('qtouch.news',array('cases'=>$cases));
    }

   public function kbs(){

           $kbs = array();
           $result1s = DB::select('SELECT a.t_id,a.t_title,a.t_shortDesc,a.t_publishDate From topic a  where a.t_channel_id = "13" order by a.t_publishDate desc');
           foreach($result1s as $key){
               $kbs[] = array(
                   'id'=>$key->t_id,
                   'title'=>$key->t_title,
                   'pDate'=>$key->t_publishDate,
               );
           }

            return view('dps.kbs',array('kbs'=>$kbs));

       }
    public function kb($id){
                $title  = $id;
                $title = str_ireplace('-',' ',$title);

               $case = array();
               $image = array();
               $result = DB::select('SELECT t_id,t_content,t_title,t_shortDesc From topic WHERE  t_title = ?',[$title]);
               $t_id = $result[0]->t_id;

               $image_url = DB::select('SELECT b.pic_filePath, b.pic_id, a.tp_sortOrder From topicPic a LEFT JOIN pic b ON a.tp_pic_id = b.pic_id WHERE  a.tp_topic_id = ?',[$t_id]);

               foreach($image_url as $key){
                   if( $key->tp_sortOrder != '999' ){
                       $image[] = array(
                                   'url'=>$key->pic_filePath,
                                   'id'=>$key->pic_id,
                                   'order'=>$key->tp_sortOrder,
                               );
                   }

               }
               $case = array(
                   'title'=>$result[0]->t_title,
                   'desc'=>$result[0]->t_shortDesc,
                   'content'=>$result[0]->t_content,
                   'images'=>$image
               );
              //$case = json_encode($case);
               return view('dps.kb',array('case'=>$case));

           }

}
