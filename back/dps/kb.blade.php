
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <link rel="stylesheet" href="/dist/css/style.css">
    <link rel="stylesheet" href="/dist/css/carousel.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
</head>
<body>
<div class="navbar-wrapper">
    <div class="container">

        <nav class="navbar navbar-inverse navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <h1 class="header">Digital publishing solution</h1>
                </div>

            </div>
        </nav>

    </div>
</div>
<article class="container">
    <div class="row">
        <div class="col-md-9">
          <h2 class="page-header">{{$case['title']}}</h2>
            <p class='img_wrap'>
                             <?php foreach($case['images'] as $img):;?>

                             <?php echo ' <img src="http://cms.qtouch.me/img/'.$img['url'].'">'; ?>
                             <?php endforeach;?>
                        </p>
            <div>
                  {!!$case['content']!!}
            </div>
        </div>
        <div class="col-md-3">
        <nav class="bs-docs-sidebar hidden-print hidden-xs hidden-sm">
                    <ul class="nav bs-docs-sidenav">

                        <li>
          <a href="#download">Download</a>
        </li>
        <li>
          <a href="#whats-included">What's included</a>
          <ul class="nav">
            <li><a href="#whats-included-precompiled">Precompiled</a></li>
            <li><a href="#whats-included-source">Source code</a></li>
          </ul>
        </li>
        <li>
          <a href="#grunt">Compiling CSS and JavaScript</a>
          <ul class="nav">
            <li><a href="#grunt-installing">Installing Grunt</a></li>
            <li><a href="#grunt-commands">Available Grunt commands</a></li>
            <li><a href="#grunt-troubleshooting">Troubleshooting</a></li>
          </ul>
        </li>
        <li>
          <a href="#template">Basic template</a>
        </li>
        <li>
          <a href="#examples">Examples</a>
          <ul class="nav">
            <li><a href="#examples-framework">Using the framework</a></li>
            <li><a href="#examples-navbars">Navbars in action</a></li>
            <li><a href="#examples-custom">Custom components</a></li>
            <li><a href="#examples-experiments">Experiments</a></li>
          </ul>
        </li>
        <li>
          <a href="#tools">Tools</a>
          <ul class="nav">
            <li><a href="#tools-bootlint">Bootlint</a></li>
          </ul>
        </li>
        <li>
          <a href="#community">Community</a>
        </li>
        <li>
          <a href="#disable-responsive">Disabling responsiveness</a>
        </li>
        <li>
          <a href="#migration">Migrating from 2.x to 3.0</a>
        </li>
        <li>
          <a href="#support">Browser and device support</a>
          <ul class="nav">
            <li><a href="#support-browsers">Supported browsers</a></li>
            <li><a href="#support-ie8-ie9">Internet Explorer 8-9</a></li>
            <li><a href="#support-ie8-respondjs">IE8 and Respond.js</a></li>
            <li><a href="#support-ie8-box-sizing">IE8 and box-sizing</a></li>
            <li><a href="#support-ie8-font-face">IE8 and @font-face</a></li>
            <li><a href="#support-ie-compatibility-modes">IE Compatibility modes</a></li>
            <li><a href="#support-ie10-width">IE10 and Windows (Phone) 8</a></li>
            <li><a href="#support-safari-percentages">Safari percent rounding</a></li>
            <li><a href="#support-fixed-position-keyboards">Modals, navbars, and virtual keyboards</a></li>
            <li><a href="#support-browser-zooming">Browser zooming</a></li>
            <li><a href="#support-sticky-hover-mobile">Sticky :hover/:focus on mobile</a></li>
            <li><a href="#support-printing">Printing</a></li>
            <li><a href="#support-android-stock-browser">Android stock browser</a></li>
            <li><a href="#support-validators">Validators</a></li>
          </ul>
        </li>
        <li>
          <a href="#third-parties">Third party support</a>
        </li>
        <li>
          <a href="#accessibility">Accessibility</a>
        </li>
        <li>
          <a href="#license-faqs">License FAQs</a>
        </li>
        <li>
          <a href="#translations">Translations</a>
        </li>


                    </ul>
                    <a class="back-to-top" href="#top">
                      Back to top
                    </a>

                  </nav>
        </div>
    </div>

</article>
  <footer class="container">
        <p class="pull-right"><a href="#">Back to top</a></p>
        <p>&copy; 2014 Company, Inc. &middot; <a href="#">About Us</a> &middot; <a href="#">Contact</a></p>
      </footer>
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="/dist/js/app.js"></script>
</body>
</html>


